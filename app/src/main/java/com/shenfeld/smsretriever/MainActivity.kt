package com.shenfeld.smsretriever

import AppSignatureHelper
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val appSignatureHelper = AppSignatureHelper(applicationContext)
        appSignatureHelper.appSignatures;
        setupListeners()
    }

    private fun setupListeners() {
        btnVerifyPhone.setOnClickListener {
            // TODO:: Send phone number to backend
            startActivity(Intent(this, CodeVerificationActivity::class.java))
        }
    }
}