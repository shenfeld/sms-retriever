package com.shenfeld.smsretriever

interface SmsListener {
    fun onSuccess(code: String)
    fun onError(errorMsg: String)
}