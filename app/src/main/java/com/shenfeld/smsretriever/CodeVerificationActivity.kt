package com.shenfeld.smsretriever

import android.content.IntentFilter
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.phone.SmsRetriever
import kotlinx.android.synthetic.main.activity_code_verification.*

class CodeVerificationActivity : AppCompatActivity() {

    val smsReceiver = SmsReceiver()

    companion object {
        private val TAG = CodeVerificationActivity::class.simpleName
        private const val SMS_RETRIEVER_STARTED = "Sms listener started!"
        private const val SMS_RETRIEVER_NOT_STARTED = "Failed to start sms retriever:"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_code_verification)
        startSmsRetriever()

        val intentFilter = IntentFilter()
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION)
        applicationContext.registerReceiver(smsReceiver, intentFilter)
    }

    private fun startSmsRetriever() {
        val client = SmsRetriever.getClient(this)
        val task = client.startSmsRetriever()

        task.addOnSuccessListener {
            Log.e(TAG, SMS_RETRIEVER_STARTED)
            listenSms()
        }

        task.addOnFailureListener { exception ->
            Log.e(TAG, "$SMS_RETRIEVER_NOT_STARTED ${exception.message}")
        }
    }

    private fun listenSms() {
        SmsReceiver.bindListener(object : SmsListener {
            override fun onSuccess(code: String) {
                // TODO:: Send the one-time code to the server
                etCode.setText(code)

            }

            override fun onError(errorMsg: String) {
                // TODO: Display error message
                makeToast(errorMsg)
            }

        })
    }

    override fun onDestroy() {
        super.onDestroy()
        applicationContext.unregisterReceiver(smsReceiver)
    }

    private fun makeToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }
}